#!/bin/bash

echo "


  ________._________________ ___         .__                       
 /  _____/|   \__    ___/   |   \   ____ |  | ______   ___________ 
/   \  ___|   | |    | /    ~    \_/ __ \|  | \____ \_/ __ \_  __ \ 
\    \_\  \   | |    | \    Y    /\  ___/|  |_|  |_> >  ___/|  | \/
 \______  /___| |____|  \___|_  /  \___  >____/   __/ \___  >__|   
        \/                    \/       \/     |__|        \/       


Welcome to GIT
"

today=$(date)
timestamp=$(date +%s)
remote="origin" 
branch="master" 
conflictPath="${timestamp}-git-conflict.txt"
SCRIPT=`realpath $0`
directory=`dirname $SCRIPT`
corpusServicesJar="../hzsk-corpus-services-0.2-jar-with-dependencies.jar"

#options=("Viewing your current GIT configuration." "Setting up your GIT configuration." "See the current state of your local GIT repository." "See the changes in the files of your local GIT repository." "Update your local GIT repository." "Save all your changes, add a message, publish your changes to the main GIT repository and update your local GIT repository." "Help!" "Quit")
options=("See the current state of your local GIT repository." "Update your local GIT repository." "Save all your changes, add a message, publish your changes to the main GIT repository and update your local GIT repository." "Viewing your current GIT configuration." "Setting up your GIT configuration." "Help!" "Quit")
PS3="
Please choose an option (1-${#options[@]}) or press ENTER to display menu: "
select opt in "${options[@]}"
do
	case $opt in
		"Viewing your current GIT configuration.")
			echo "Your username is:"	
			git config user.name
			echo "Your email is:"
			git config user.email
			;;
		"Setting up your GIT configuration.")
			read -p "Enter your user name: " usrname
					 git config --global user.name "\"$usrname\""
			read -p "Enter your email: " usrmail
					 git config --global user.email "\"$usrmail\""
			;;
		"See the current state of your local GIT repository.")
			git status
			;;
		#"See the changes in the files of your local GIT repository.")
		# 	if [[ $(git diff) ]]; then
		#		echo "To close the following list of differences press 'q'"
		#		git diff
		#	else
		#		echo "There are no changes in your local files."
		#	fi
		#	read -n 1 -s -r -p "Press any key to continue"
		#	;;
		"Update your local GIT repository.")
			CONFLICTS=$(git ls-files -u | wc -l)
			if [ "$CONFLICTS" -gt 0 ]
				then
					echo "The local GIT repository cannot be updated because of a GIT conflict." >> $conflictPath
					echo "Please resolve merge conflict manually." >> $conflictPath
					read
					exit 1
				else
					if [ -z "$(git status --porcelain)" ] 
													then
														echo "There are no local changes, updating will be started." 
														git fetch
														git pull $remote $branch 
														CONFLICTS=$(git ls-files -u | wc -l)
														if [ "$CONFLICTS" -gt 0 ]
															then
																echo "There is a merge conflict. Aborting"
																git merge --abort
																echo "Please resolve merge conflict manually in $directory." >> $conflictPath
																read
														else
																echo "There are no merge conflicts, updating will be carried out." 
														fi
														if [ -z "$(git status --porcelain)" ] 
															then
																echo "Everything was successful." 
														else
																git status
																echo "The pull was faulty. Please fix it. "
																git status >> $conflictPath
																echo "The pull was faulty. Please fix it. " >> $conflictPath
																read																
														fi	
													else									            								            
														git status
														echo "There are local changes. Please remove or save them with the GIT Helper before updating."
														read														
					fi	
						
			fi
			;;	
		"Save all your changes, add a message, publish your changes to the main GIT repository and update your local GIT repository.")
			CONFLICTS=$(git ls-files -u | wc -l)
			if [ "$CONFLICTS" -gt 0 ]
				then
					echo "Please resolve merge conflict manually." >> $conflictPath
					read
					exit 1
				else
					echo "No merge conflict to begin with."
						#show all the files that are changed and ask if they should be added
						echo "The files that are changed are:"
						git status
						read -p "Do you want to add these changes to the main GIT repository? (y/n)" yn
						case $yn in
							[Yy]* ) 
									while true; do
										read -p "Enter your commit message: " message		
										echo "Your commit message is: $message"
										read -p "Is the message correct? (y/n)" yn2
										case $yn2 in
											[YyJj]* )	break;;
											[Nn]* )  	echo "Please enter the message again";;		
											* ) 		echo "Please answer yes or no.";;
										esac
									done		
									git add -A
									git commit -m "$message"
									#read -p "The files will be formatted automatically, do you want to proceed? (y/n)" yn
									#case $yn in
									#	[Yy]* ) java -Xmx3g -jar $corpusServicesJar -i $directory -o $directory/prettyprint-output.html -c PrettyPrintData -f
												java -Xmx3g -jar $corpusServicesJar -i $directory -o $directory/prettyprint-output.html -c PrettyPrintData -f
												git add -A 
												git reset -- curation/CorpusServices_Errors.xml
												git checkout curation/CorpusServices_Errors.xml
												git commit -am "Automatically pretty printed on $today" 
												git fetch
												git pull $remote $branch 
												CONFLICTS=$(git ls-files -u | wc -l)
												if [ "$CONFLICTS" -gt 0 ]
													then
														echo "There is a merge conflict. Aborting"
														git merge --abort
														echo "Please resolve merge conflict manually in $directory." >> $conflictPath
														echo "The process was stopped and GIT Helper will be closed."
														read
														exit
												else
														echo "Merging was successful or not needed." 
														git push $remote $branch
												fi
												if [ -z "$(git status --porcelain)" ] 
													then
														echo "Everything was successful." 
														read	
												else
														git status
														echo "The pull was faulty. Please fix it. "
														git status >> $conflictPath
														echo "The pull was faulty. Please fix it. " >> $conflictPath
														read		
												fi	
								#				break;;		
								#		[Nn]* ) echo "The process was stopped and GIT Helper will be closed."
								#				read
								#				exit;;
								#		* ) echo "Please answer yes or no.";;
								#	esac											
									;;		
							[Nn]* ) echo "The process was stopped.";;
							* ) echo "Please answer yes or no.";;
						esac				
			fi
			;;
		"Help!")
			clear
			echo "This script can be used to add changes you made to the main GIT repository so everyone working with the data can see them."
			echo "Please press ENTER to display the menu and enter the number of the option you want to use."
			echo "If there is a conflict or something goes wrong, please contact the technical team."
            echo " "
            echo "GitHelper version: 1.1"
            echo "Git version:" 
            git --version            
            echo "Java version:" 
            java -version
            echo "corpus-services version: " $corpusServicesJar
            if [ -f "$corpusServicesJar" ]; then
                echo "$corpusServicesJar exists"
            else 
                echo "ERROR: $corpusServicesJar does not exist"
            fi
			;;
		"Quit")
			clear
			break
			;;
		*) echo "The option $REPLY is not available";;
	esac
done




