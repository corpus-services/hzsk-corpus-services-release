#!/bin/bash

echo "


  ________._________________ ___         .__                       
 /  _____/|   \__    ___/   |   \   ____ |  | ______   ___________ 
/   \  ___|   | |    | /    ~    \_/ __ \|  | \____ \_/ __ \_  __ \ 
\    \_\  \   | |    | \    Y    /\  ___/|  |_|  |_> >  ___/|  | \/
 \______  /___| |____|  \___|_  /  \___  >____/   __/ \___  >__|   
        \/                    \/       \/     |__|        \/       


Welcome to GIT
"

today=$(date)
timestamp=$(date +%s)
remote="origin" 
branch="master" 
conflictPath="${timestamp}-git-conflict.txt"
SCRIPT=`realpath $0`
directory=`dirname $SCRIPT`
corpusServicesJar="../hzsk-corpus-services-0.2-jar-with-dependencies.jar"

#options=("Viewing your current GIT configuration." "Setting up your GIT configuration." "See the current state of your local GIT repository." "See the changes in the files of your local GIT repository." "Update your local GIT repository." "Save all your changes, add a message, publish your changes to the main GIT repository and update your local GIT repository." "Help!" "Quit")
options=("Aktuellen Stand des lokalen GIT repository anschauen." "Lokales GIT repository auf den neuesten Stand bringen." "Alle ausgeführten Änderungen speichern, eine Nachricht hinzufügen, diese Änderungen beim main GIT repository veröffentlichen und das lokale GIT repository auf den neuesten Stand bringen." "Die aktuelle GIT Konfiguration ansehen." "Die GIT Konfiguration ändern." "Hilfe!" "Beenden")
PS3="
Bitte wähle eine Option (1-${#options[@]}) oder drücke ENTER um das Menü anzuzeigen: "
select opt in "${options[@]}"
do
	case $opt in
		"Die aktuelle GIT Konfiguration ansehen.")
			echo "Name:"	
			git config user.name
			echo "Email-Adresse:"
			git config user.email
			;;
		"Die GIT Konfiguration ändern.")
			read -p "Vorname Nachname eingeben: " usrname
					 git config --global user.name "\"$usrname\""
			read -p "Email-Adresse eingeben: " usrmail
					 git config --global user.email "\"$usrmail\""
			;;
		"Aktuellen Stand des lokalen GIT repository anschauen.")
			git status
			;;
		#"See the changes in the files of your local GIT repository.")
		# 	if [[ $(git diff) ]]; then
		#		echo "To close the following list of differences press 'q'"
		#		git diff
		#	else
		#		echo "There are no changes in your local files."
		#	fi
		#	read -n 1 -s -r -p "Press any key to continue"
		#	;;
		"Lokales GIT repository auf den neuesten Stand bringen.")
			CONFLICTS=$(git ls-files -u | wc -l)
			if [ "$CONFLICTS" -gt 0 ]
				then
					echo "Das lokale GIT repository kann nicht auf den neuesten Stand gebracht werden, weil ein GIT-Konflikt vorliegt." >> $conflictPath
					echo "Der GIT-Konflikt muss manuell gelöst werden." >> $conflictPath
					read
					exit 1
				else
					if [ -z "$(git status --porcelain)" ] 
													then
														echo "Es liegen keine lokalen Änderungen vor, das updaten wird vorbereitet." 
														git fetch
														git pull $remote $branch 
														CONFLICTS=$(git ls-files -u | wc -l)
														if [ "$CONFLICTS" -gt 0 ]
															then
																echo "Es liegt ein GIT-Konflikt vor. Der Vorgang wird abgebrochen."
																git merge --abort
																echo "Der GIT-Konflikt muss manuell gelöst werden in $directory." >> $conflictPath
																read
														else
																echo "Es liegt kein GIT-Konflikt vor, das GIT repository wird auf den neuesten Stand gebracht." 
														fi
														if [ -z "$(git status --porcelain)" ] 
															then
																echo "Der Vorgang war erfolgreich." 
														else
																git status
																echo "Achtung: Der pull konnte nicht durchgeführt werden. "
																git status >> $conflictPath
																echo "Achtung: Der pull konnte nicht durchgeführt werden. " >> $conflictPath
																read																
														fi	
													else									            								            
														git status
														echo "Es liegen lokale Änderungen vor. Diese müssen zuerst entfernt oder mithilfe des GIT Assistenten gespeichert werden, bevor das GIT repository auf den neuesten Stand gebracht werden kann."
														read														
					fi	
						
			fi
			;;	
		"Alle ausgeführten Änderungen speichern, eine Nachricht hinzufügen, diese Änderungen beim main GIT repository veröffentlichen und das lokale GIT repository auf den neuesten Stand bringen.")
			CONFLICTS=$(git ls-files -u | wc -l)
			if [ "$CONFLICTS" -gt 0 ]
				then
					echo "Der GIT-Konflikt muss manuell gelöst werden." >> $conflictPath
					read
					exit 1
				else
					echo "No merge conflict to begin with."
						#show all the files that are changed and ask if they should be added
						echo "The files that are changed are:"
						git status
						read -p "Sollen folgende geänderte Dateien im main GIT repository veröffentlicht werden? (j/n)" yn
						case $yn in
							[YyJj]* ) 
									while true; do
										read -p "Commit Nachricht eingeben: " message		
										echo "Die Commit Nachricht ist: $message"
										read -p "Ist die Nachricht korrekt? (j/n)" yn2
										case $yn2 in
											[YyJj]* )	break;;
											[Nn]* )  	echo "Bitte Nachricht erneut eingeben";;		
											* ) 		echo "Bitte mit j (ja) oder n (nein) antworten.";;
										esac
									done		
									git add -A
									git commit -m "$message"
									#read -p "The files will be formatted automatically, do you want to proceed? (y/n)" yn
									#case $yn in
									#	[YyJj]* ) java -Xmx3g -jar $corpusServicesJar -i $directory -o $directory/prettyprint-output.html -c PrettyPrintData -f
												java -Xmx3g -jar $corpusServicesJar -i $directory -o $directory/prettyprint-output.html -c PrettyPrintData -f
												git add -A 
												git reset -- curation/CorpusServices_Errors.xml
												git checkout curation/CorpusServices_Errors.xml
												git commit -am "Automatically pretty printed on $today" 
												git fetch
												git pull $remote $branch 
												CONFLICTS=$(git ls-files -u | wc -l)
												if [ "$CONFLICTS" -gt 0 ]
													then
														echo "Es liegt ein GIT-Konflikt vor. Der Vorgang wird abgebrochen."
														git merge --abort
														echo "Der GIT-Konflikt muss manuell gelöst werden in $directory." >> $conflictPath
														echo "Der Vorgang wurde abgebrochen und der GIT Assistent wird geschlossen."
														read
														exit
												else
														echo "Merging war erfolgreich bzw. nicht nötig." 
														git push $remote $branch
												fi
												if [ -z "$(git status --porcelain)" ] 
													then
														echo "Der Vorgang war erfolgreich." 
														read	
												else
														git status
														echo "Achtung: Der pull konnte nicht durchgeführt werden. "
														git status >> $conflictPath
														echo "Achtung: Der pull konnte nicht durchgeführt werden. " >> $conflictPath
														read		
												fi	
								#				break;;		
								#		[Nn]* ) echo "The process was stopped and GIT Helper will be closed."
								#				read
								#				exit;;
								#		* ) echo "Please answer yes or no.";;
								#	esac											
									;;		
							[Nn]* ) echo "Der Vorgang wurde abgebrochen.";;
							* ) echo "Bitte mit j (ja) oder n (nein) antworten.";;
						esac				
			fi
			;;
		"Hilfe!")
			clear
			echo "Dieses Skript kann genutzt werden, um Änderungen, die lokal gemacht wurden, dem main GIT repository hinzuzufügen, sodass alle, die mit den Daten arbeiten, sie sehen und nutzen können. "
			echo "Bitte ENTER drücken um das Menü anzuzeigen und die Nummer des Vorgangs, der ausgeführt werden soll, eingeben und mit ENTER bestätigen. "
			echo "Wenn ein GIT-Konflikt oder ein Fehler auftritt bitte beim technischen Team melden. "
			echo " "
            echo "GitHelper Version: 1.1"
            echo "Git Version:" 
            git --version            
            echo "Java Version:" 
            java -version
            echo "Corpus-Services version: " $corpusServicesJar
            if [ -f "$corpusServicesJar" ]; then
                echo "$corpusServicesJar wurde gefunden. "
            else 
                echo "FEHLER: $corpusServicesJar wurde nicht gefunden. "
            fi
			;;
		"Beenden")
			clear
			break
			;;
		*) echo "Die Option $REPLY ist nicht verfügbar.";;
	esac
done




